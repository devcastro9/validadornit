﻿using Microsoft.EntityFrameworkCore;
using ValidadorNIT.Models;

namespace ValidadorNIT.Data;

public partial class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<GcBeneficiario> GcBeneficiarios { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Metodo base
        base.OnModelCreating(modelBuilder);
        // Api fluent
        modelBuilder.Entity<GcBeneficiario>(entity =>
        {
            entity.ToTable("gc_beneficiario", tb => tb.HasTrigger("trigger_actualiza_beneficiario"));
            entity.Property(e => e.ActivoSin).HasDefaultValueSql("((0))");
            entity.Property(e => e.Analizado).HasDefaultValueSql("((0))");
            entity.Property(e => e.BeneficiarioDeudor).IsFixedLength();
            entity.Property(e => e.ColaRevision).HasDefaultValueSql("((0))");
            entity.Property(e => e.EstadoCodigo)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.EstadoCodigoContrato)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.RazonSocialSin).HasDefaultValueSql("('-')");
            entity.Property(e => e.ValidoSin).HasDefaultValueSql("((0))");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
