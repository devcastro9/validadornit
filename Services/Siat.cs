﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using ValidadorNIT.Utility;
using ValidadorNIT.ViewModels;

namespace ValidadorNIT.Services
{
    public class Siat
    {
        private readonly HttpClient _httpClient = null!;
        //private readonly int _retardo = 5;
        public Siat(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(@"http://pbdw.impuestos.gob.bo:8080/gob.sin.padron.servicio.web/consulta/");
        }
        /// <summary>
        /// Funcion para verificar si un NIT es valido.
        /// </summary>
        /// <param name="nit">Valor numerico del NIT</param>
        /// <returns>Retorna la respuesta en forma de objeto</returns>
        public async Task<VerificacionNit?> VerificarNit(long nit)
        {
            try
            {
                // Ejecutamos la peticion
                HttpResponseMessage response = await _httpClient.GetAsync($"verificarContribuyente?nit={nit}");
                // Retardo
                //await Task.Delay(_retardo);
                // Generamos una excepcion si el codigo HTTP indica un error
                response.EnsureSuccessStatusCode();
                // Deserializamos
                JsonSerializerOptions options = new()
                {
                    ReferenceHandler = ReferenceHandler.IgnoreCycles
                };
                options.Converters.Add(new FormatDateTimeNullConverter("dd/MM/yyyy"));
                string result = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<VerificacionNit>(result, options);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
