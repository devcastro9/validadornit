﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ValidadorNIT.Data;
using ValidadorNIT.Models;
using ValidadorNIT.Services;
using ValidadorNIT.ViewModels;

namespace ValidadorNIT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GcBeneficiariosController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly Siat _siat;

        public GcBeneficiariosController(AppDbContext context, Siat siat)
        {
            _context = context;
            _siat = siat;
        }

        // GET: api/GcBeneficiarios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GcBeneficiario>> GetGcBeneficiario(string id)
        {
            var gcBeneficiario = await _context.GcBeneficiarios.FindAsync(id);
            if (gcBeneficiario == null)
            {
                return NotFound();
            }
            return gcBeneficiario;
        }

        // PUT: api/GcBeneficiarios/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        public async Task<IActionResult> PutGcBeneficiario()
        {
            // Reseteo de datos SIN
            _ = await _context.Database.ExecuteSqlRawAsync(@"EXECUTE [dbo].[facValidacionNIT]");
            // Comienza el proceso
            List<GcBeneficiario> result = await _context.GcBeneficiarios.Where(m => m.ValidoSin == true).ToListAsync();
            foreach (var gcBeneficiario in result)
            {
                bool valido = long.TryParse(gcBeneficiario.BeneficiarioNit, out long nit);
                if (valido)
                {
                    VerificacionNit? getResponse = await _siat.VerificarNit(nit);
                    if (getResponse != null)
                    {
                        // Caso de respuesta positiva
                        gcBeneficiario.BeneficiarioNit = nit.ToString();
                        if (getResponse.Ok)
                        {
                            gcBeneficiario.RazonSocialSin = getResponse.RazonSocial;
                            gcBeneficiario.ActivoSin = getResponse.Estado == "ACTIVO";
                        }
                        else
                        {
                            gcBeneficiario.ValidoSin = false;
                            gcBeneficiario.ActivoSin = false;
                            gcBeneficiario.RazonSocialSin = (from m in getResponse.Mensajes select m.Descripcion).First();
                        }
                    }
                    else
                    {
                        gcBeneficiario.ValidoSin = false;
                        gcBeneficiario.ActivoSin = false;
                        gcBeneficiario.RazonSocialSin = "RESP_NULA";
                    }
                    // Guardado
                    _context.Entry(gcBeneficiario).State = EntityState.Modified;
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!GcBeneficiarioExists(gcBeneficiario.BeneficiarioCodigo))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
            }
            return Ok();
        }

        private bool GcBeneficiarioExists(string id)
        {
            return _context.GcBeneficiarios.Any(e => e.BeneficiarioCodigo == id);
        }
    }
}
