﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ValidadorNIT.ViewModels
{
    public class VerificacionNit
    {
        public VerificacionNit()
        {
            Mensajes = new HashSet<VerificacionMensaje>();
        }
        [JsonPropertyName("ok")]
        public bool Ok { get; set; }
        [JsonPropertyName("mensajes")]
        public virtual ICollection<VerificacionMensaje> Mensajes { get; set; }
        [JsonPropertyName("nit")]
        public long? Nit { get; set; }
        [JsonPropertyName("razonSocial")]
        public string? RazonSocial { get; set; }
        [JsonPropertyName("estado")]
        public string? Estado { get; set; }
        [JsonPropertyName("fechaUltimoEstado")]
        public DateTime? FechaUltimoEstado { get; set; }
    }
}
