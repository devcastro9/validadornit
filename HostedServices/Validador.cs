﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ValidadorNIT.Data;
using ValidadorNIT.Models;
using ValidadorNIT.Services;
using ValidadorNIT.ViewModels;

namespace ValidadorNIT.HostedServices
{
    public class Validador : IHostedService, IDisposable
    {
        private readonly IServiceScopeFactory _scopeFactory;
        public readonly Siat _siat;
        private readonly ILogger<Validador> _logger;
        private Timer? _timer;
        public Validador(IServiceScopeFactory scopeFactory, Siat siat, ILogger<Validador> logger)
        {
            _scopeFactory = scopeFactory;
            _siat = siat;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted service facturador iniciando.");
            _timer = new Timer(Validar, null, TimeSpan.FromSeconds(0), TimeSpan.FromMinutes(30));
            return Task.CompletedTask;
        }
        private async void Validar(object? state)
        {
            int contador = 0;
            int numeroLote = 50;
            _logger.LogInformation("Validacion en ejecucion");
            using (var ambito = _scopeFactory.CreateScope())
            {
                var _context = ambito.ServiceProvider.GetRequiredService<AppDbContext>();
                // Analisis Completo
                _ = await _context.GcBeneficiarios.ExecuteUpdateAsync(m => m.SetProperty(p => p.Analizado, p => false));
                // Reseteo de datos SIN
                _ = await _context.Database.ExecuteSqlRawAsync(@"EXECUTE [dbo].[facValidacionNIT]");
                // Inicio de Validacion
                List<GcBeneficiario> result = await _context.GcBeneficiarios.Where(m => m.Analizado == false && m.ColaRevision == true).ToListAsync();
                foreach (var gcBeneficiario in result)
                {
                    bool valido = long.TryParse(gcBeneficiario.BeneficiarioNit, out long nit);
                    if (valido)
                    {
                        VerificacionNit? getResponse = await _siat.VerificarNit(nit);
                        if (getResponse != null)
                        {
                            // Caso de respuesta positiva
                            gcBeneficiario.BeneficiarioNit = nit.ToString();
                            if (getResponse.Ok)
                            {
                                gcBeneficiario.ValidoSin = true;
                                gcBeneficiario.RazonSocialSin = getResponse.RazonSocial;
                                gcBeneficiario.ActivoSin = getResponse.Estado == "ACTIVO";
                            }
                            else
                            {
                                gcBeneficiario.ValidoSin = false;
                                gcBeneficiario.ActivoSin = false;
                                gcBeneficiario.RazonSocialSin = (from m in getResponse.Mensajes select m.Descripcion).First();
                            }
                        }
                        else
                        {
                            gcBeneficiario.ValidoSin = false;
                            gcBeneficiario.ActivoSin = false;
                            gcBeneficiario.RazonSocialSin = "RESP_NULA";
                        }
                    }
                    else
                    {
                        gcBeneficiario.ValidoSin = false;
                        gcBeneficiario.ActivoSin = false;
                        gcBeneficiario.RazonSocialSin = "NO-VALIDO";
                    }
                    gcBeneficiario.Analizado = true;
                    _context.Entry(gcBeneficiario).State = EntityState.Modified;
                    contador++;
                    // Guardado de cambios por lote
                    if (contador % numeroLote == 0)
                    {
                        try
                        {
                            await _context.SaveChangesAsync();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                // Guardado de cambios fuera del lote
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            _logger.LogInformation("Validacion terminada");
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted Service facturador deteniendose.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _timer?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
